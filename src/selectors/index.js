export const numberIndexesSelector = (state) => Object.keys(state.data.results)
  .filter((number) => {
    const { results } = state.data.search;

    if (!results.length) {
      return true;
    }

    return results.includes(number);
  });

export const numbersSelector = (state) => 
  numberIndexesSelector(state).reduce((result, key) => {
    result[key] = state.data.results[key];

    return result;
  }, {});

export const numberSelector = (state) => state.currentNumber ? numbersSelector(state)[state.currentNumber] : {};

export const numberTypesSelector = (state) => Object.values(numberSelector(state));
import React from 'react';
import { useStore } from '../../store';
import { numberTypesSelector } from '../../selectors';
import { numberDescriptions } from '../../utils'
import './style.css';

const Dashboard = ({ onScrollEnd }) => {
  const { state, dispatch } = useStore();
  const types = numberTypesSelector(state);

  const onSettingsClick = () => {
    dispatch({ type: 'openModal', payload: 'settings' });
  };

  return (
    <div className="dashboard">
      <button onClick={onSettingsClick}>Settings</button>
      {state.data.fetching && (
        <div className="dashboard">Loading...</div>
      )}
      {!state.data.fetching && types.map(({ type, number, text }) => {
        const descriptionDecorator = numberDescriptions[type];
        const description = descriptionDecorator(number, text);

        return <div key={type} className="fact">{description}</div>;
      })}
      {!types.length && (
        <div className="dashboard">Nothing found!</div>
      )}
    </div>
  );
};

export default Dashboard;
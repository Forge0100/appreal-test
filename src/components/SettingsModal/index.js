import React, { useState } from 'react';
import { useStore } from '../../store';
import Modal from '../Modal';
import './style.css';

const SettingsModal = () => {
  const { state, dispatch } = useStore();
  const [limit, setLimit] = useState(state.data.limit);

  const onLimitChange = (event) => {
    setLimit(event.target.value);
  }

  const onCloseClick = () => {
    dispatch({ type: 'closeModal' });
  }

  const handleSubmit = (event) => {
    event.preventDefault();

    dispatch({ type: 'setLimit', payload: parseInt(limit) });
    onCloseClick();
  }

  if (state.modal !== 'settings') {
    return null;
  }

  return (
    <Modal onClose={onCloseClick}>
      <div className="container">
        <form onSubmit={handleSubmit}>
          <div className="row">
            <div className="col-25">
              <label htmlFor="limit">Limit</label>
            </div>
            <div className="col-75">
              <input type="text" id="limit"  value={limit} onChange={onLimitChange} />
            </div>
          </div>
          <div className="row">
            <input type="submit" value="Save" />
          </div>
        </form>
      </div>
    </Modal>
  );
};

export default SettingsModal;
export const numberDescriptions = {
    trivia: (number, fact) => `Trivia fact for ${number}: ${fact}`,
    date: (number, fact) => `Date fact for ${number}: ${fact}`,
    math: (number, fact) => `Math fact for ${number}: ${fact}`,
};
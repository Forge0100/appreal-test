import React, { useEffect, useReducer } from 'react';
import { Context, initialState, reducer } from './store';
import { numberIndexesSelector } from './selectors';
import Navigation from './components/Navigation';
import Dashboard from './components/Dashboard';
import SettingsModal from './components/SettingsModal';
import './App.css';

const logReducer = (reducer) => (state, action) => {
  const nextState = reducer(state, action);

  console.log('state', state);
  console.log('nextState', nextState);
  console.log('action', action);

  return nextState;
};

function App() {
  const [state, dispatch] = useReducer(logReducer(reducer), initialState);
  const numberIndexes = numberIndexesSelector(state);  

  const loadNumbers = () => {
    const offset = numberIndexes.length;
    const rangeString = `${offset}..${offset + state.data.limit}`;

    try {
      dispatch({ type: 'setNumbersRequest' });

      fetch(`http://numbersapi.com/${rangeString}/${state.defaultType}?json`)
      .then(response => response.json())
      .then(response => {
        const payload = Object.keys(response).reduce((result, key) => {
          const { number, text, type } = response[key];
          
          result[key] = {
            [type]: {
              number,
              text,
              type
            }
          };

          return result;
        }, {});

        dispatch({ type: 'setNumbersSuccess', payload });
      });
    } catch (e) {
      dispatch({ type: 'setNumbersFailure' });
    }
  };

  useEffect(() => {
    loadNumbers();
  }, []);

  return (
    <Context.Provider value={{ state, dispatch }}>
      <div className="App">
        <SettingsModal />
        
        <Navigation onScrollEnd={loadNumbers} />
        <Dashboard />
      </div>
    </Context.Provider>
  );
}

export default App;

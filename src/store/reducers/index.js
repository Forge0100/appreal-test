export const initialState = {
    currentNumber: null,
    defaultType: 'trivia',
    types: ['trivia', 'date', 'math'],
    modal: null,
    data: {
        results: {},
        search: {
          results: [],
          text: '',
        },
        fetching: false,
        limit: 15,
    },
};

export const reducer = (state, action) => {
    switch (action.type) {
      case 'setCurrentNumber':
        return { ...state, currentNumber: action.payload };
      case 'setNumberRequest':
      case 'setNumberFailure':
      case 'setNumbersRequest':
      case 'setNumbersFailure':
        return {
          ...state,
          data: {
            ...state.data,
            fetching: true,
          }
        };
      case 'setNumberSuccess':
        return {
          ...state,
          data: {
            ...state.data,
            fetching: false,
            results: {
              ...Object.keys(state.data.results).map((number) => ({
                ...state.data.results[number],
                ...(action.payload[number] || {})
              }), {})
            }
          }
        };
      case 'setNumbersSuccess':
        return { 
          ...state,
          data: {
            ...state.data,
            fetching: false,
            results: {
              ...state.data.results,
              ...action.payload
            }
          }
        };
      case 'search': {
        const results = Object.entries(state.data.results)
          .filter(([number, types]) => types[state.defaultType].text.concat(number).includes(action.payload))
          .map(([number]) => number);

        return {
          ...state,
          currentNumber: initialState.currentNumber,
          data: {
            ...state.data,
            search: {
              text: action.payload,
              results
            }
          }
        };
      }
      case 'openModal':
        return {
          ...state,
          modal: action.payload
        }
      case 'closeModal':
        return {
          ...state,
          modal: initialState.modal
        }
      case 'setLimit':
        return {
          ...state,
          data: {
            ...state.data,
            limit: action.payload
          }
        }
      default:
        return state;
    }
}
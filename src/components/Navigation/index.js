import React, { useRef } from 'react';
import PropTypes from 'prop-types';
import { useStore } from '../../store';
import { numberIndexesSelector, numbersSelector } from '../../selectors';
import './style.css';

const Navigation = ({ onScrollEnd }) => {
  const listRef = useRef(null);
  const { state, dispatch } = useStore();
  const { currentNumber, types, data, defaultType } = state;
  const numberIndexes = numberIndexesSelector(state);
  const numbers = numbersSelector(state);

  const loadNumber = (number, type) => {
    try {
      dispatch({ type: 'setNumberRequest' });

      fetch(`http://numbersapi.com/${number}/${type}?json`)
      .then(response => response.json())
      .then(({ type, number, text }) => {
        const payload = {
          [number]: {
            [type]: {
              number,
              text,
              type
            }
          }
        };

        dispatch({ type: 'setNumberSuccess', payload });
      });
    } catch(e) {
      dispatch({ type: 'setNumberFailure' });
    }
  };

  const onSearch = (event) => {
    const { value } = event.target;

    dispatch({ type: 'search', payload: value });
  };

  const handleScroll = () => {
    const { scrollTop, scrollHeight, clientHeight } = listRef.current;

    if (clientHeight + Math.round(scrollTop) !== scrollHeight || data.fetching || data.search.text) return;

    onScrollEnd();
  };

  const selectNumber = (number) => () => {
    dispatch({ type: 'setCurrentNumber', payload: number });

    const loadingTypes = types.filter(type => type !== defaultType);

    loadingTypes.forEach(type => {
      loadNumber(number, type);
    });
  }

  return (
    <div className="navigation">
      <input 
        type="text" 
        placeholder="Filter..."
        className="filter"
        onChange={onSearch}
      />
      <ul
        ref={listRef}
        className="list"
        onScroll={handleScroll}
      >
        {numberIndexes.map((number) => {
          const { text } = numbers[number][defaultType];
  
          return (
            <li
              key={number}
              className={number === currentNumber ? 'selected': ''}
              title={text}
              onClick={selectNumber(number)}
            >
              {number}
            </li>
          );
        })}
      </ul>
    </div>
  );
};

Navigation.propTypes = {
  onScrollEnd: PropTypes.func.isRequired
};

export default Navigation;